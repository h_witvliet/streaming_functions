import numpy as np
cimport numpy as np
import cython

ctypedef np.float64_t DTYPE_t 

ctypedef struct COMPLEX_t:
    np.float64_t elt
    np.int64_t rmin
    np.int64_t rmax

@cython.boundscheck(False)
def compress_0(np.ndarray[DTYPE_t] nparray not None, int n, int N=0):
    """compress a simple array to n elements

    Parameters
    ----------
    nparray : array
        numerical values with observations
    n : int
        required size of output
    N : int, optional
        no of elements in nparray. defaults to nparray.size

    Returns
    -------
    ndarray
        new numpy ndarray of n rows with (elt, rmin(e), rmax(e)), where rmin
        and rmax are the minimum and maximum possible rank of e Also, nparray
        will be sorted!
    """
    assert nparray.dtype == np.float64
    if N == 0:
        N = nparray.size
    #
    dtype = [('elt', np.float64), ('rmin', np.int64), ('rmax', np.int64)]
    cdef np.ndarray[COMPLEX_t] res = np.zeros(n, dtype=dtype)
    #
    nparray.sort()
    cdef np.float64_t frac
    cdef int i, elt
    frac = float(N - 1) / (n - 1)
    for i in range(n):
        elt = int(i * frac)
        res[i].elt = nparray[elt]
        res[i].rmin = elt + 1
        res[i].rmax = elt + 1
    # 
    #res[-1].elt = nparray[-1]
    #res[-1].rmin = N
    #res[-1].rmax = N
    return res

def compress(np.ndarray[COMPLEX_t] esummary not None, int n):
    """compress an epsilon-summary to a smaller summary of size n

    Parameters
    ----------
    esummary : ndarray (dtype, int64, int64)
        an epsilon summary of a dataset
    n : int
        required size

    Returns
    --------
    ndarray (dtype, int64, int64)
        a new numpy ndarray with the smaller epsilon-summary
    """
    cdef np.ndarray[COMPLEX_t] res = np.zeros(n, dtype=esummary.dtype)
    cdef np.float64_t frac = float(esummary['rmax'][-1] - 1) / (n - 1)
    cdef np.float64_t rank = 1
    cdef int index = 0
    cdef int i
    cdef int until = esummary.size
    for i in range(until):
        if esummary[i].rmax >= rank:
            res[index] = esummary[i]
            index += 1 
            rank += frac
    #res[-1] = esummary[-1]
    return res

def merge(np.ndarray[COMPLEX_t] esummaryA not None, np.ndarray[COMPLEX_t] esummaryB not None):
    """merge to epsilon-summaries from two disjunct datasets

    Parameters
    ----------
    esummaryA : ndarray (dtype, int64, int64)
        epsilon summary of first dataset
    esummaryB : ndarray (dtype, int64, int64)
        epsilon summary of second dataset

    Returns
    --------
    ndarray (dtype, int64, int64)
        epsilon-summary of combined dataset
    """
    cdef int indexA, indexB, i, rmin, rmax
    cdef COMPLEX_t elt
    indexA = indexB = 0
    cdef int sizeA = esummaryA.size
    cdef int sizeB = esummaryB.size
    res = np.zeros(sizeA + sizeB, dtype=esummaryA.dtype)
    cdef int until = res.size
    for i in range(until):
        if indexB == sizeB or (indexA < sizeA and esummaryA[indexA].elt < esummaryB[indexB].elt):
            elt = esummaryA[indexA]
            rmin = elt.rmin + (0 if indexB == 0 else esummaryB[indexB-1].rmin)
            rmax = elt.rmax + (esummaryB[indexB].rmax - 1 if indexB < sizeB else esummaryB[indexB-1].rmax)
            indexA += 1
        else:
            elt = esummaryB[indexB]
            rmin = elt.rmin + (0 if indexA == 0 else esummaryA[indexA-1].rmin)
            rmax = elt.rmax + (esummaryA[indexA].rmax - 1 if indexA < sizeA else esummaryA[indexA-1].rmax)
            indexB += 1
        res[i].elt = elt.elt
        res[i].rmin = rmin
        res[i].rmax = rmax
    return res

