import numpy as np
from math import log, floor, ceil

"""
    approximate quantile estimates based on:
    
    A Fast Algorithm for Approximate Quantiles in High Speed Data Streams, Qi Zhang and Wei Wang
    Power-Conserving Computation of Order-Statistics over Sensor Networks, Michael B. Greenwald and Sanjeev Khanna

    Notation:
    N total size
    epsilon (small) error
    b block size,
    b = floor(log2(epsilon * N)) / epsilon

"""

def compress_0(nparray, n, N=None):
    """compress a simple array to n elements

    Parameters
    ----------
    nparray : array
        numerical values with observations
    n : int
        required size of output
    N : int, optional
        no of elements in nparray. defaults to nparray.size

    Returns
    -------
    ndarray
        new numpy ndarray of n rows with (elt, rmin(e), rmax(e)), where rmin
        and rmax are the minimum and maximum possible rank of e Also, nparray
        will be sorted!
    """
    if N is None:
        N = nparray.size
    #
    dtype = [('elt', nparray.dtype), ('rmin', np.int64), ('rmax', np.int64)]
    res = np.zeros(n, dtype=dtype)
    #
    nparray.sort()
    frac = float(N - 1) / (n - 1)
    elts = np.asarray(np.arange(n) * frac, dtype=np.int)
    res['elt'] = nparray[elts]
    res['rmin'] = elts + 1
    res['rmax'] = elts + 1
    # 
    #res[-1] = (nparray[-1], N, N)
    return res

def compress(esummary, n):
    """compress an epsilon-summary to a smaller summary of size n

    Parameters
    ----------
    esummary : ndarray (dtype, int64, int64)
        an epsilon summary of a dataset
    n : int
        required size

    Returns
    --------
    ndarray (dtype, int64, int64)
        a new numpy ndarray with the smaller epsilon-summary
    """
    res = np.zeros(n, dtype=esummary.dtype)
    frac = float(esummary['rmax'][-1] - 1) / (n - 1)
    rank = 1
    index = 0
    for i in range(esummary.size - 1):
        if esummary[i]['rmax'] >= rank:
            res[index] = esummary[i]
            index += 1 
            rank += frac
    res[-1] = esummary[-1]
    return res

def merge(esummaryA, esummaryB):
    """merge to epsilon-summaries from two disjunct datasets

    Parameters
    ----------
    esummaryA : ndarray (dtype, int64, int64)
        epsilon summary of first dataset
    esummaryB : ndarray (dtype, int64, int64)
        epsilon summary of second dataset

    Returns
    --------
    ndarray (dtype, int64, int64)
        epsilon-summary of combined dataset
    """
    indexA = indexB = 0
    res = np.zeros(esummaryA.size + esummaryB.size, dtype=esummaryA.dtype)
    for i in range(res.size):
        if indexB == esummaryB.size or (indexA < esummaryA.size and esummaryA['elt'][indexA] < esummaryB['elt'][indexB]):
            elt = esummaryA[indexA]
            rmin = elt['rmin'] + (0 if indexB == 0 else esummaryB[indexB-1]['rmin'])
            rmax = elt['rmax'] + (esummaryB[indexB]['rmax'] - 1 if indexB < esummaryB.size else esummaryB[indexB-1]['rmax'])
            indexA += 1
        else:
            elt = esummaryB[indexB]
            rmin = elt['rmin'] + (0 if indexA == 0 else esummaryA[indexA-1]['rmin'])
            rmax = elt['rmax'] + (esummaryA[indexA]['rmax'] - 1 if indexA < esummaryA.size else esummaryA[indexA-1]['rmax'])
            indexB += 1
        res[i] = (elt['elt'], rmin, rmax)
    return res

def quantile(esummary, perc):
    """returns the estimated quantile

    Parameters
    ----------
    esummary : (dtype, int64, int64)
        epsilon summary of dataset
    perc : float
        required quantile

    Returns
    --------
    dtype
        value of estimated quantile
    """
    rank = perc * esummary['rmax'][-1]
    for i in esummary:
        if i['rmax'] >= rank:
            return i['elt']
    return None

def quantiles(esummary, percs):
    """returns the estimated quantile

    Parameters
    ----------
    esummary : (dtype, int64, int64)
        epsilon summary of dataset
    perc : list or array
        array of required quantiles

    Returns
    --------
    array of dtype
        array of estimated quantiles
    """
    # binary search via rmax should be much faster!
    if type(percs) != np.ndarray:
        percs = np.array(percs)
    rank = percs * esummary['rmax'][-1]
    index = 0
    res = np.zeros(percs.size, dtype=esummary['elt'].dtype)
    for i in esummary:
        if i['rmax'] >= rank[index]:
            res[index] = i['elt']
            index += 1
            if index == res.size:
                return res


class Estimated_Quantiles(object):
    """estimates for the case where the size of the dataset is known"""

    def __init__(self, N, epsilon, dtype):
        """initialization

        Parameters
        ----------
        N : int
            exact size of the dataset
        epsilon : float
            measure for the accuracy
        dtype : np type
            numpy dtype of elements
        """
        self.dtype = dtype
        self.N = N
        self.count = 0
        self.epsilon = epsilon
        self.b = int(floor(log(epsilon * N)/(log(2) * epsilon)))
        self.n = int(ceil(self.b/2)) + 1
        self.s0 = np.zeros(self.b, dtype=dtype)
        self.Ns0 = 0
        self.S = [None]
        self.summary = None

    def update(self, elt):
        """use elt to update the info for the multi-level structure

        Parameters
        ----------
        elt : dtype
            value of dtype with new observation of dataset
        """
        self.s0[self.Ns0] = elt
        self.Ns0 += 1
        self.count += 1
        if self.Ns0 == self.b:
            self.updateS0()

    def updateArray(self, array):
        """update the multilevel with all elements from the array

        Parameters
        ----------
        array : array
            numpy array with the new observations
        """
        self.count += array.size
        start = 0
        while True:
            # calculate next block that can be processed
            if start == array.size:
                break
            remainder = self.b - self.Ns0
            if start + remainder <= array.size:
                self.s0[self.Ns0:] = array[start: start+remainder]
                self.Ns0 += remainder
                self.updateS0()
                start += remainder
            else:
                size = array.size - start
                self.s0[self.Ns0: self.Ns0 + size] = array[start:]
                self.Ns0 += size
                start += size

        for elt in array:
            self.s0[self.Ns0] = elt
            self.Ns0 += 1
            if self.Ns0 == self.b:
                self.updateS0()

    def updateS0(self):
        """s0 is filled. So compress s0 and update other levels of S as neccessary"""

        sc = compress_0(self.s0, self.n)
        self.Ns0 = 0
        # pad the list of epsilon estimates with an empty block if neccesary
        # so we can process the last block
        if self.S[-1] is not None:
            self.S.append(None)
        for i in range(len(self.S)):
            if self.S[i] is None:
                self.S[i] = sc
                break
            else:
                sc = compress(merge(self.S[i], sc), self.n)
                self.S[i] = None

    def finish(self):
        """combine all epsilon estimates in S to enable getting the quantiles"""

        # process s0
        if self.Ns0 > 0:
            sc = compress_0(self.s0, self.Ns0)
        else:
            sc = None
        # and combine all calculated estimates
        for e_est in self.S:
            if e_est is None:
                continue
            if sc is None:
                sc = e_est
            else:
                sc = merge(e_est, sc)
        self.summary = sc

    def quantiles(self, percs):
        """return the required quantiles

        Parameters
        ----------
        percs : list or array
            array of percentages
        """

        if self.summary is None:
            self.finish()
        return quantiles(self.summary, percs)


class Estimated_Quantiles_Streaming(object):
    """streaming version of approximate quantile estimates"""

    def __init__(self, epsilon, dtype):
        """initialization

        Parameters
        ----------
        epsilon : float
            measure for the accuracy
        dtype : array
            numpy dtype of elements
        """
        self.dtype = dtype
        self.epsilon = epsilon
        self.block = 1
        self.N = int(2 ** self.block / self.epsilon)
        self.n = int(2/epsilon)
        self.S = []
        self.summary = None
        self.working = Estimated_Quantiles(self.N, self.epsilon, self.dtype)
        self.dirty = True

    def update(self, elt):
        self.dirty = True
        self.working.update(elt)
        if self.working.N == self.working.count:
            # finish working block
            self.working.finish()
            self.S.append(compress(self.working.summary, self.n))
            # and start a new block
            self.block += 1
            self.N = int(2 ** self.block / self.epsilon)
            self.working = Estimated_Quantiles(self.N, self.epsilon, self.dtype)

    def updateArray(self, array):
        """update the multilevel with all elements from the array

        Parameters
        ----------
        array : array
            numpy array with the new observations
        """
        self.dirty = True
        start = 0
        while True:
            if start == array.size:
                break
            #
            remainder = self.working.N - self.working.count
            if start + remainder <= array.size:
                # we can complete the current input block
                self.working.updateArray(array[start: start + remainder])
                self.working.finish()
                self.S.append(compress(self.working.summary, self.n))
                # and start a new block
                self.block += 1
                self.N = int(2 ** self.block / self.epsilon)
                self.working = Estimated_Quantiles(self.N, self.epsilon, self.dtype)
                start += remainder
            else:
                size = array.size - start
                self.working.updateArray(array[start:])
                start += size

        for elt in array:
            self.update(elt)
    
    def finish(self):
        """prepare the estimates to give the approximate quantiles"""
        self.working.finish()
        sc = self.working.summary
        for si in self.S:
            if sc is None:
                sc = si
            else:
                sc = merge(si, sc)
        self.summary = sc
        self.dirty = False

    def quantiles(self, percs):
        """return the required quantiles

        Parameters
        ----------
        percs : list or array
            array of percentages
        """

        if self.summary is None or self.dirty:
            self.finish()
        return quantiles(self.summary, percs)

